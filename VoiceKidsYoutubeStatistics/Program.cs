﻿namespace VoiceKidsYoutubeStatistics
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Google.Apis.Services;
    using Google.Apis.YouTube.v3;
    using Microsoft.Extensions.Configuration;

    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private async Task Run()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("config.json")
                .Build();

            // example: get view count for a video by id
            /*var req = youtubeService.Videos.List("statistics");
            req.Id = "9Wl3wrs4-DQ,bZugmOaC4J8"; // video id
            var res = await req.ExecuteAsync();
            var video = res.Items.ElementAt(0).Statistics.ViewCount;*/

            var seasons = config.GetSection("playlists").Get<string[]>();
            if (seasons == null || seasons.All(s => string.IsNullOrWhiteSpace(s)))
            {
                Console.WriteLine($"Error: no seasons found.");
                return;
            }

            var saveTo = config["saveto"];
            if (!Directory.Exists(saveTo))
            {
                Console.WriteLine($"Error: directory {saveTo} does not exist. Create and try again.");
                return;
            }
            var path = Path.Combine(saveTo, DateTime.Now.ToString("dd.MM.yyyy"));
            var apiKey = config["apikey"];

            if (string.IsNullOrWhiteSpace(apiKey))
            {
                Console.WriteLine($"Error: your API KEY is empty.");
                return;
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApplicationName = "youtube",
                ApiKey = apiKey
            });

            var tasks = new Task<List<Tuple<string, ulong>>>[seasons.Length];
            for (var i = 0; i < seasons.Length; i++)
            {
                var id = seasons[i];
                var number = i + 1;
                tasks[i] = Task.Run(() => ProcessSeason(youtubeService, id, number, path));
            }

            IOrderedEnumerable<Tuple<string, ulong>> allSeasons = null;
            try
            {
                allSeasons = (await Task.WhenAll(tasks))
                    .SelectMany(i => i).OrderByDescending(d => d.Item2);
            }
            catch (Exception)
            {
                Console.WriteLine($"Error: looks like your API KEY is not valid or network is not available.");
                return;
            }

            var sb = new StringBuilder();
            foreach (var item in allSeasons)
            {
                sb.AppendLine($"{item.Item1} : {item.Item2}");
            }

            File.WriteAllText($@"{path}\TOP.txt", sb.ToString());
            Console.WriteLine($"Done. Check your files at {path}");
        }

        private async Task<List<Tuple<string, ulong>>> ProcessSeason(
            YouTubeService youtubeService, string seasonId, int seasonNumber, string saveTo)
        {
            var currentPage = 1;
            string next = null;
            var result = new List<Tuple<string, ulong>>(); // title, view count

            var seasonDetails = youtubeService.Playlists.List("contentDetails");
            seasonDetails.Id = seasonId;
            var list = await seasonDetails.ExecuteAsync();
            foreach (var item in list.Items)
                Console.WriteLine($"season {seasonNumber} has {item.ContentDetails.ItemCount} videos");

            while (true)
            {
                var playlistPageRequest = youtubeService.PlaylistItems.List("snippet,contentDetails");
                playlistPageRequest.PlaylistId = seasonId;
                playlistPageRequest.MaxResults = 10;
                if (next != null) playlistPageRequest.PageToken = next;
                var page = await playlistPageRequest.ExecuteAsync();
                next = page.NextPageToken;

                var ids = string.Join(",", page.Items.Select(i => i.ContentDetails.VideoId));
                var videosRequest = youtubeService.Videos.List("snippet,contentDetails,statistics"); ;
                videosRequest.Id = ids;
                var videos = await videosRequest.ExecuteAsync();

                foreach (var video in videos.Items)
                {
                    result.Add(Tuple.Create($"{video.Snippet.Title} ({video.Id}) - {video.Statistics.LikeCount}",
                        video.Statistics.ViewCount.Value));
                }

                Console.WriteLine($"season {seasonNumber} page {currentPage++} processed");
                if (next == null) break;
            }

            var sb = new StringBuilder();
            foreach (var item in result.OrderByDescending(u => u.Item2))
            {
                sb.AppendLine($"{item.Item1} : {item.Item2}");
            }

            File.WriteAllText($@"{saveTo}\season_{seasonNumber}.txt", sb.ToString());

            return result;
        }
    }
}
